package com.yuchang.dataencrypt.util;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;
import cn.hutool.extra.spring.SpringUtil;
import com.yuchang.dataencrypt.config.EncryptProperties;

/**
 * @author yurisu
 * @description 加密工具类
 * @since 2024/3/24 22:47
 */
public class SecurityUtil {

    public static String encrypt(String plainText) {
        if (CharSequenceUtil.isBlank(plainText)) {
            return plainText;
        }
        AES security = getSecurity();
        return security.encryptBase64(plainText);
    }

    public static String decrypt(String plainText) {
        if (CharSequenceUtil.isBlank(plainText)) {
            return plainText;
        }
        AES security = getSecurity();
        return security.decryptStr(plainText);
    }

    private static AES getSecurity() {
        EncryptProperties properties = SpringUtil.getBean(EncryptProperties.class);
        return SecureUtil.aes(properties.getKey().getBytes());
    }
}
