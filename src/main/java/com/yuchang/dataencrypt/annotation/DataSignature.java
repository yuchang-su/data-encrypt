package com.yuchang.dataencrypt.annotation;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.yuchang.dataencrypt.serializer.DecryptSerializer;
import com.yuchang.dataencrypt.serializer.EncryptSerializer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author yurisu
 * @description 数据加密注解
 * @since 2024/3/24 22:38
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@JacksonAnnotationsInside
// 使用自定义的序列化实现
@JsonSerialize(using = EncryptSerializer.class)
// 使用自定义的序列化实现
@JsonDeserialize(using = DecryptSerializer.class)
public @interface DataSignature {
}
