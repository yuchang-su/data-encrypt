package com.yuchang.dataencrypt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yurisu
 */
@SpringBootApplication
public class DataEncryptApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataEncryptApplication.class, args);
    }

}
