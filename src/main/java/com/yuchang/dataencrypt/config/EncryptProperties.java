package com.yuchang.dataencrypt.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author yurisu
 */
@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "yuchang.encrypt")
public class EncryptProperties {

    private String key = "yuchang";

}
