package com.yuchang.dataencrypt.serializer;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.yuchang.dataencrypt.util.SecurityUtil;
import io.micrometer.common.util.StringUtils;

import java.io.IOException;

/**
 * @author yurisu
 * @description 反序列化实现
 * @since 2024/3/24 23:05
 */
public class DecryptSerializer extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JacksonException {
        // 数据不为空时加密数据
        String valueAsString = jsonParser.getValueAsString();
        if (StringUtils.isBlank(valueAsString)) {
            return valueAsString;
        }
        return SecurityUtil.decrypt(valueAsString);
    }
}
