package com.yuchang.dataencrypt.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.yuchang.dataencrypt.util.SecurityUtil;
import io.micrometer.common.util.StringUtils;

import java.io.IOException;

/**
 * @author yurisu
 * @description 加密序列化实现
 * @since 2024/3/24 22:41
 */
public class EncryptSerializer extends JsonSerializer<String> {

    @Override
    public void serialize(String s, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        // 数据不为空时加密数据
        if (StringUtils.isBlank(s)) {
            return;
        }
        String encrypt = SecurityUtil.encrypt(s);
        jsonGenerator.writeString(encrypt);
    }
}
